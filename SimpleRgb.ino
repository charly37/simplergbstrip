//Libraries definitions

class color
{
public:
  void goToColor();
  void setRandome();
  void displayLed();
  color();
  void goToColor(color aDestination);
  void goToRandColor();
  void printDebug();
  bool operator==(const color &other) const;
  bool operator!=(const color &other) const;
  
  //friend ostream& operator<< (ostream &out, color &iColor);
  
private:
  int _red;
  int _blue;
  int _green;
};

//pin guirlande led
const int _OutPinLedBlue = 9;
const int _OutPinLedGreen = 10;
const int _OutPinLedRed = 11;

color _color;
//Time between each delta color change when the process is drifting from one color to another
int aDriftSpeed=50;
//Time the process wait when it reach a color....before chossing a new color and drifting into it.
int aColorDuration=10000;



void color::printDebug()
{
  Serial.print("_red:");
  Serial.print(_red);
  Serial.print("_blue:");
  Serial.print(_blue);
  Serial.print("_green:");
  Serial.print(_green);
  Serial.println("");
}

/*ostream& operator<< (ostream &out, color &iColor)
{
    // Since operator<< is a friend of the Point class, we can access
    // Point's members directly.
    out << "(" << iColor._red << ", " <<
        iColor._blue << ", " <<
        iColor._green << ")";
    return out;
}*/

color::color()
{
  this->setRandome();
}

void color::setRandome()
{
  this->_red = random(0, 255);
  this->_blue = random(0, 255);
  this->_green = random(0, 255);
}

void color::displayLed()
{
  analogWrite(_OutPinLedBlue, this->_blue); // impulsion largeur voulue sur la broche 0 = 0% et 255 = 100% haut
  analogWrite(_OutPinLedGreen, this->_green); // impulsion largeur voulue sur la broche 0 = 0% et 255 = 100% haut
  analogWrite(_OutPinLedRed, this->_red); // impulsion largeur voulue sur la broche 0 = 0% et 255 = 100% haut
}

bool color::operator==(const color &other) const 
{
  return ((this->_red==other._red)&&(this->_blue==other._blue)&&(this->_green==other._green));
}

bool color::operator!=(const color &other) const 
{
  return !(*this == other);
}

void color::goToColor(color aDestination)
{
}

void color::goToRandColor()
{
  color aDestColor;
  aDestColor.setRandome();
  Serial.println("Going to color : ");
  aDestColor.printDebug();
  while (*this != aDestColor)
  {
    Serial.println("Current Color : ");
    this->printDebug();
    
    if(this->_red!=aDestColor._red)
    {
      //Serial.println("Update RED");
      if(this->_red<aDestColor._red)
      {
        //Serial.println("Increase RED");
        this->_red++;
      }
      else
      {
        //Serial.println("Decrease RED");
        this->_red--;
      }
    }
    if(this->_blue!=aDestColor._blue)
    {
      //Serial.println("Update BLUE");
      if(this->_blue<aDestColor._blue)
      {
        //Serial.println("Increase BLUE");
        this->_blue++;
      }
      else
      {
        //Serial.println("Decrease BLUE");
        this->_blue--;
      }
    }
    if(this->_green!=aDestColor._green)
    {
      //Serial.println("Update GREEN");
      if(this->_green<aDestColor._green)
      {
        //Serial.println("Increase GREEN");
        this->_green++;
      }
      else
      {
        //Serial.println("Decrease GREEN");
        this->_green--;
      }
    }
    this->displayLed();
    delay(aDriftSpeed);
  }
}

void RandomLedColor() 
{
  Serial.println("RandomLedColor");
  int randNumberGreen = random(0, 255);
  int randNumberRed = random(0, 255);
  int randNumberBlue = random(0, 255);

  analogWrite(_OutPinLedBlue, randNumberGreen); // impulsion largeur voulue sur la broche 0 = 0% et 255 = 100% haut
  analogWrite(_OutPinLedGreen, randNumberRed); // impulsion largeur voulue sur la broche 0 = 0% et 255 = 100% haut
  analogWrite(_OutPinLedRed, randNumberBlue); // impulsion largeur voulue sur la broche 0 = 0% et 255 = 100% haut
}

void setup()
{
  //defined IO
  pinMode(_OutPinLedBlue, OUTPUT);
  pinMode(_OutPinLedGreen, OUTPUT);
  pinMode(_OutPinLedRed, OUTPUT);

  digitalWrite(_OutPinLedBlue, LOW);
  digitalWrite(_OutPinLedGreen, LOW);
  digitalWrite(_OutPinLedRed, LOW);

  delay(aColorDuration);
}

void loop()
{
  //This command allow the master to turn the light on
  _color;
  _color.goToRandColor();
  //aMyColor.displayLed();
  //RandomLedColor();
  delay(5000);
}





